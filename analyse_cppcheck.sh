#!/bin/bash

# https://blog.kitware.com/static-checks-with-cmake-cdash-iwyu-clang-tidy-lwyu-cpplint-and-cppcheck/

# http://cppcheck.net/manual.html#CMake

mkdir -p build/CppCheck
cd build/CppCheck
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..
cppcheck --project=compile_commands.json --addon=cert.py --addon=threadsafety.py
