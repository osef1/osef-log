#ifndef OSEFDEBUG_H
#define OSEFDEBUG_H

#include <cstring>  // strerror
#include <iostream>  // cout cerr

#ifndef DT
    #ifdef DEBUG
        #define DT(t) t.tv_sec << "," << std::setw(9) << std::setfill('0') << t.tv_nsec
    #else
        #define DT(t)
    #endif
#endif

#ifndef DOUT
    #ifdef DEBUG
        #define DOUT(s) std::cout << s << std::endl;
    #else
        #define DOUT(s)
    #endif
#endif

#ifndef DHEX
    #ifdef DEBUG
        #define DHEX(s) std::cout << std::hex << s << std::dec << std::endl;
    #else
        #define DHEX(s)
    #endif
#endif

#ifndef DERR
    #ifdef DEBUG
        #define DERR(s) std::cerr << "\033[31;40m" << __LINE__ << " " << __PRETTY_FUNCTION__ << " " << s << "\033[0;0m" << std::endl;
    #else
        #define DERR(s)
    #endif
#endif

#ifndef DWARN
    #ifdef DEBUG
        #define DWARN(s) std::cerr << "\033[33;40m" << __LINE__ << " " << __PRETTY_FUNCTION__ << " " << s << "\033[0;0m" << std::endl;
    #else
        #define DWARN(s)
    #endif
#endif

#ifndef DERX
    #ifdef DEBUG
        #define DERX(s) std::cerr << "\033[31;40m" << __LINE__ << " " << __PRETTY_FUNCTION__ << " " << std::hex << s << std::dec << "\033[0;0m" << std::endl;
    #else
        #define DERX(s)
    #endif
#endif

#endif /* OSEFDEBUG_H */
